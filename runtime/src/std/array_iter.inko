#! Iterator types and methods for the `Array` type.
#!
#! These types and methods are defined separately as otherwise `std::iterator`
#! would depend on `std::array` while `std::array` would depend on
#! `std::iterator`.
import std::iterator::(self, Iterator)

impl Array!(T) {
  ## Returns an `Iterator` that iterates over all values in `self`.
  ##
  ## # Examples
  ##
  ## Iterating over an `Array`:
  ##
  ##     let numbers = Array.new(10, 20, 30)
  ##     let iter = numbers.iter
  ##
  ##     iter.next # => 10
  ##     iter.next # => 20
  ##     iter.next # => 30
  ##     iter.next # => Nil
  def iter -> Iterator!(T) {
    iterator.index_enumerator(length) do (index) {
      self[index]
    }
  }
}
