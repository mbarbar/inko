import std::byte_array::ByteArray
import std::conversion::ToString
import std::fs::path::Path
import std::fs::raw::(READ_APPEND, READ_ONLY, READ_WRITE)
import std::fs::raw::(self, MODIFIED_AT, APPEND_ONLY, WRITE_ONLY)
import std::process
import std::test
import std::test::assert
import test::features
import test::fixtures::(VALID_DIRECTORY, VALID_FILE)
import test::fixtures::(self, INVALID_TIME, INVALID_DIRECTORY, INVALID_FILE)

object DummyFile {}

def remove(path: ToString) -> Nil {
  try raw.remove(path.to_string) else Nil
}

test.group('std::fs::raw.open') do (g) {
  g.test('Opening a valid file') {
    assert.no_panic {
      try! raw.open(
        type: DummyFile,
        path: VALID_FILE.to_string,
        mode: READ_ONLY
      )
    }
  }

  g.test('Opening an invalid file') {
    assert.panic {
      try! raw.open(
        type: DummyFile,
        path: INVALID_FILE.to_string,
        mode: READ_ONLY
      )
    }
  }
}

test.group('std::fs::raw.mode_for_write') do (g) {
  g.test('Obtaining the mode for opening a file as write-only') {
    assert.equal(raw.mode_for_write, WRITE_ONLY)
  }

  g.test('Obtaining the mode for opening a file as append-only') {
    assert.equal(raw.mode_for_write(append: True), APPEND_ONLY)
  }
}

test.group('std::fs::raw.mode_for_read_write') do (g) {
  g.test('Obtaining the mode for opening a file as read-write') {
    assert.equal(raw.mode_for_read_write, READ_WRITE)
  }

  g.test('Obtaining the mode for opening a file as read-append') {
    assert.equal(raw.mode_for_read_write(append: True), READ_APPEND)
  }
}

test.group('std::fs::raw.copy') do (g) {
  g.test('Copying a valid file') {
    let orig = VALID_FILE
    let copy = fixtures.temporary_file_path

    process.defer {
      remove(copy)
    }

    try! raw.copy(from: orig.to_string, to: copy.to_string)

    assert.true(raw.file?(copy.to_string))
  }

  g.test('Copying an invalid file') {
    let orig = INVALID_FILE
    let copy = fixtures.temporary_file_path
    let size = try raw.copy(from: orig.to_string, to: copy.to_string) else -1

    assert.equal(size, -1)
  }
}

test.group('std::fs::raw.size') do (g) {
  g.test('Obtaining the size of a valid file') {
    let size = try raw.size(VALID_FILE.to_string) else -1

    assert.greater(size, 0)
  }

  g.test('Obtaining the size of an invalid file') {
    let size = try raw.size(INVALID_FILE.to_string) else -1

    assert.equal(size, -1)
  }
}

test.group('std::fs::raw.remove') do (g) {
  g.test('Removing a valid file') {
    let path = fixtures.temporary_file_path.to_string

    try! raw.open(type: DummyFile, path: path, mode: WRITE_ONLY)
    try! raw.remove(path)

    assert.false(raw.file?(path))
  }

  g.test('Removing an invalid file') {
    assert.panic {
      try! raw.remove(INVALID_FILE.to_string)
    }
  }
}

test.group('std::fs::raw.file?') do (g) {
  g.test('Checking if a valid file is a file') {
    assert.true(raw.file?(VALID_FILE.to_string))
  }

  g.test('Checking if an invalid file is a file') {
    assert.false(raw.file?(INVALID_FILE.to_string))
  }

  g.test('Checking if a valid directory is a file') {
    assert.false(raw.file?(VALID_DIRECTORY.to_string))
  }

  g.test('Checking if an invalid directory is a file') {
    assert.false(raw.file?(INVALID_DIRECTORY.to_string))
  }
}

test.group('std::fs::raw.directory?') do (g) {
  g.test('Checking if a valid file is a directory') {
    assert.false(raw.directory?(VALID_FILE.to_string))
  }

  g.test('Checking if an invalid file is a directory') {
    assert.false(raw.directory?(INVALID_FILE.to_string))
  }

  g.test('Checking if a valid directory is a directory') {
    assert.true(raw.directory?(VALID_DIRECTORY.to_string))
  }

  g.test('Checking if an invalid directory is a directory') {
    assert.false(raw.directory?(INVALID_DIRECTORY.to_string))
  }
}

test.group('std::fs::raw.exists?') do (g) {
  g.test('Checking if a valid file exists') {
    assert.true(raw.exists?(VALID_FILE.to_string))
  }

  g.test('Checking if an invalid file exists') {
    assert.false(raw.exists?(INVALID_FILE.to_string))
  }

  g.test('Checking if a valid directory exists') {
    assert.true(raw.exists?(VALID_DIRECTORY.to_string))
  }

  g.test('Checking if an invalid directory exists') {
    assert.false(raw.exists?(INVALID_DIRECTORY.to_string))
  }
}

test.group('std::fs::raw.file_time') do (g) {
  g.test('Obtaining the modification time of a valid file') {
    let time = try {
      raw.file_time(path: VALID_FILE.to_string, kind: MODIFIED_AT)
    } else {
      INVALID_TIME
    }

    assert.not_equal(time, INVALID_TIME)
  }
}

features.creation_time?.if_true {
  test.group('std::fs::raw.created_at') do (g) {
    g.test('Obtaining the creation time of a valid file') {
      let time = try raw.created_at(VALID_FILE.to_string) else INVALID_TIME

      assert.not_equal(time, INVALID_TIME)
    }

    g.test('Obtaining the creation time of an invalid file') {
      let time = try raw.created_at(INVALID_FILE.to_string) else INVALID_TIME

      assert.equal(time, INVALID_TIME)
    }
  }
}

features.modification_time?.if_true {
  test.group('std::fs::raw.modified_at') do (g) {
    g.test('Obtaining the modification time of a valid file') {
      let time = try raw.modified_at(VALID_FILE.to_string) else INVALID_TIME

      assert.not_equal(time, INVALID_TIME)
    }

    g.test('Obtaining the modification time of an invalid file') {
      let time = try raw.modified_at(INVALID_FILE.to_string) else INVALID_TIME

      assert.equal(time, INVALID_TIME)
    }
  }
}

features.access_time?.if_true {
  test.group('std::fs::raw.accessed_at') do (g) {
    g.test('Obtaining the access time of a valid file') {
      let time = try raw.accessed_at(VALID_FILE.to_string) else INVALID_TIME

      assert.not_equal(time, INVALID_TIME)
    }

    g.test('Obtaining the access time of an invalid file') {
      let time = try raw.accessed_at(INVALID_FILE.to_string) else INVALID_TIME

      assert.equal(time, INVALID_TIME)
    }
  }
}

test.group('std::fs::raw.read_bytes') do (g) {
  g.test('Reading a valid number of bytes from a file') {
    let file = try! raw.open(
      type: DummyFile,
      path: VALID_FILE.to_string,
      mode: READ_ONLY
    )

    let bytes = ByteArray.new
    let read = try! raw.read_bytes(file: file, bytes: bytes, size: 2)

    assert.equal(bytes.length, 2)
    assert.equal(read, 2)
  }

  g.test('Reading an invalid number of bytes from a file') {
    assert.panic {
      let file = try! raw.open(
        type: DummyFile,
        path: VALID_FILE.to_string,
        mode: READ_ONLY
      )

      let bytes = ByteArray.new
      let read = try! raw.read_bytes(file: file, bytes: bytes, size: -1)
    }
  }
}

test.group('std::fs::raw.write_string') do (g) {
  g.test('Writing a String to a file') {
    let path = fixtures.temporary_file_path.to_string

    process.defer {
      remove(path)
    }

    let file = try! raw.open(type: DummyFile, path: path, mode: WRITE_ONLY)
    let written = try! raw.write_string(file: file, data: 'abc')

    assert.equal(written, 3)
  }

  g.test('Writing a String to a file that is opened in read-only mode') {
    let file = try! raw.open(
      type: DummyFile,
      path: VALID_FILE.to_string,
      mode: READ_ONLY
    )

    let written = try raw.write_string(file: file, data: 'abc') else -1

    assert.equal(written, -1)
  }
}

test.group('std::fs::raw.write_bytes') do (g) {
  g.test('Writing a ByteArray to a file') {
    let path = fixtures.temporary_file_path.to_string

    process.defer {
      remove(path)
    }

    let file = try! raw.open(type: DummyFile, path: path, mode: WRITE_ONLY)
    let written = try! raw.write_bytes(file: file, bytes: 'abc'.to_byte_array)

    assert.equal(written, 3)
  }

  g.test('Writing a ByteArray to a file that is opened in read-only mode') {
    let file = try! raw.open(
      type: DummyFile,
      path: VALID_FILE.to_string,
      mode: READ_ONLY
    )

    let written = try {
      raw.write_bytes(file: file, bytes: 'abc'.to_byte_array)
    } else {
      -1
    }

    assert.equal(written, -1)
  }
}

test.group('std::fs::raw.flush') do (g) {
  g.test('Flushing any pending writes to disk') {
    assert.no_panic {
      let path = fixtures.temporary_file_path.to_string

      process.defer {
        remove(path)
      }

      let file = try! raw.open(type: DummyFile, path: path, mode: WRITE_ONLY)

      try! raw.flush(file)
    }
  }
}

test.group('std::fs::raw.seek') do (g) {
  g.test('Seeking to a valid position') {
    let file = try! raw.open(
      type: DummyFile,
      path: VALID_FILE.to_string,
      mode: READ_ONLY
    )

    let pos = try! raw.seek(file: file, position: 4)

    assert.equal(pos, 4)
  }
}
